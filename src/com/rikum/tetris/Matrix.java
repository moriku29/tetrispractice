package com.rikum.tetris;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import com.rikum.tetris.util.Board;

public class Matrix {

	Board<Mino> board;
	Queue<Mino> nextList;
	Board<Mino> currentTetromino;
	Orientation orientation;

	int[][][] threeWideSRSOffset = { { 
		{ 0, 0 }, { -1, 0 }, { -1, +1 }, { 0, -2 }, { -1, -2 } }, // up -> right
			{ { 0, 0 }, { +1, 0 }, { +1, -1 }, { 0, +2 }, { +1, +2 } }, // right -> down
			{ { 0, 0 }, { +1, 0 }, { +1, +1 }, { 0, -2 }, { +1, -2 } }, // down -> left
			{ { 0, 0 }, { -1, 0 }, { -1, -1 }, { 0, +2 }, { -1, +2 } } // left -> up
	};

	public Matrix() {
		board = new Board<Mino>(10, 40, Mino.BLANK);
		nextList = getNextMinoList();
		orientation = Orientation.UP;
	}

	public Matrix(int x, int y) {
		// TODO Auto-generated constructor stub
		board = new Board<Mino>(x, y, Mino.BLANK);
	}

	public Mino getValue(int x, int y) {
		// TODO Auto-generated method stub
		return board.getValue(x, y);
	}

	public Queue<Mino> getNextMinoList() {
		// TODO Auto-generated method stub
		if (nextList == null) {
			nextList = new LinkedList<>();
		}
		if (nextList.size() >= 7) {
			return nextList;
		}

		ArrayList<Mino> inputterNextBag = new ArrayList<>();
		Random r = new Random();
		while (inputterNextBag.size() < 7) {
			Mino selectedMino = null;
			int a = r.nextInt(7);
			switch (a) {
			case 0:
				selectedMino = Mino.SKY;
				break;
			case 1:
				selectedMino = Mino.BLUE;
				break;
			case 2:
				selectedMino = Mino.ORANGE;
				break;
			case 3:
				selectedMino = Mino.PURPLE;
				break;
			case 4:
				selectedMino = Mino.YELLOW;
				break;
			case 5:
				selectedMino = Mino.RED;
				break;
			case 6:
				selectedMino = Mino.GREEN;
				break;
			}
			if (!inputterNextBag.contains(selectedMino)) {
				inputterNextBag.add(selectedMino);
			}
		}
		nextList.addAll(inputterNextBag);

		return nextList;
	}

	public void setMino(int x, int y, Mino mino) {
		// TODO Auto-generated method stub
		board.setValue(x, y, mino);
	}

	public void setCurrentTetromino(Board<Mino> mino) {
		// TODO Auto-generated method stub
		currentTetromino = mino;
	}

	public boolean isColliding() {
		// TODO Auto-generated method stub
		for (int i = currentTetromino.getOffset()[0]; i < currentTetromino.getSize()[0]
				+ currentTetromino.getOffset()[0]; i++) {
			for (int j = currentTetromino.getOffset()[1]; j < currentTetromino.getSize()[1]
					+ currentTetromino.getOffset()[1]; j++) {
				if (currentTetromino.getValue(i, j) == null || currentTetromino.getValue(i, j) != Mino.BLANK
						&& (board.getValue(i, j) == null || board.getValue(i, j) != Mino.BLANK)) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean place() {
		// TODO Auto-generated method stub
		if (this.isColliding())
			return false;
		for (int i = currentTetromino.getOffset()[0]; i < currentTetromino.getSize()[0]
				+ currentTetromino.getOffset()[0]; i++) {
			for (int j = currentTetromino.getOffset()[1]; j < currentTetromino.getSize()[1]
					+ currentTetromino.getOffset()[1]; j++) {
				if (currentTetromino.getValue(i, j) != Mino.BLANK && board.getValue(i, j) != null
						&& board.getValue(i, j) == Mino.BLANK) {
					board.setValue(i, j, currentTetromino.getValue(i, j));
				}
			}
		}

		return true;
	}

	public void performHardDrop() throws Exception {
		// TODO Auto-generated method stub
		while (this.moveTetrominoDown())
			;
		if (this.isColliding()) {
			throw new Exception();
		}
		this.place();
	}

	public Board<Mino> getCurrentTetromino() {
		// TODO Auto-generated method stub
		return currentTetromino;
	}

	public boolean moveTetrominoRight() {
		// TODO Auto-generated method stub
		// Board<Mino> newPlacement = currentTetromino;
		currentTetromino.setOffset(currentTetromino.getOffset()[0] + 1, currentTetromino.getOffset()[1]);
		if (isColliding()) {
			currentTetromino.setOffset(currentTetromino.getOffset()[0] - 1, currentTetromino.getOffset()[1]);
			return false;
		}
		return true;
	}

	public boolean moveTetrominoLeft() {
		// TODO Auto-generated method stub
		currentTetromino.setOffset(currentTetromino.getOffset()[0] - 1, currentTetromino.getOffset()[1]);
		if (isColliding()) {
			currentTetromino.setOffset(currentTetromino.getOffset()[0] + 1, currentTetromino.getOffset()[1]);
			return false;
		}
		return true;
	}

	public boolean moveTetrominoDown() {
		// TODO Auto-generated method stub
		currentTetromino.setOffset(currentTetromino.getOffset()[0], currentTetromino.getOffset()[1] - 1);
		if (isColliding()) {
			currentTetromino.setOffset(currentTetromino.getOffset()[0], currentTetromino.getOffset()[1] + 1);
			return false;
		}
		return true;
	}

	public void turnTetrominoRight() {
		// TODO Auto-generated method stub
		Board<Mino> copy = currentTetromino.copy();
		currentTetromino.rotateCounterClockwise();
		Orientation originalOrientation = orientation;
		int[] minoOffset = new int[2];
		minoOffset[0] = currentTetromino.getOffset()[0];
		minoOffset[1] = currentTetromino.getOffset()[1];

		int orientationOffset = -1;
		switch (orientation) {
		case UP:
			orientation = Orientation.RIGHT;
			orientationOffset = 0;
			break;
		case DOWN:
			orientation = Orientation.LEFT;
			orientationOffset = 2;
			break;
		case LEFT:
			orientation = Orientation.UP;
			orientationOffset = 3;
			break;
		case RIGHT:
			orientation = Orientation.DOWN;
			orientationOffset = 1;
			break;
		}
		if (orientationOffset == -1) {
			currentTetromino = copy;
			orientation = originalOrientation;
			return;

		}

		for (int i = 0; i < threeWideSRSOffset[0].length; i++) {
			System.out.println("DEBUG: originalOffset = ["
					+ Integer.toString(minoOffset[0]) + ", "
					+ Integer.toString(minoOffset[1]) + "]");
			currentTetromino.setOffset(minoOffset[0] + this.threeWideSRSOffset[orientationOffset][i][0],
					minoOffset[1] + this.threeWideSRSOffset[orientationOffset][i][1]);
			System.out.println("DEBUG: offset = ["
					+ Integer.toString(currentTetromino.getOffset()[0]) + ", "
					+ Integer.toString(currentTetromino.getOffset()[1]) + "]");
			if (!this.isColliding()) {
				return;
			}
		}

		// reaching up to this point means that every possible srs position collides
		this.currentTetromino = copy;
		this.orientation = originalOrientation;
		return;

	}

	public void turnTetrominoLeft() {
		// TODO Auto-generated method stub
		Board<Mino> copy = currentTetromino.copy();

		Orientation originalOrientation = orientation;
		int[] minoOffset = new int[2];
		minoOffset[0] = currentTetromino.getOffset()[0];
		minoOffset[1] = currentTetromino.getOffset()[1];
		currentTetromino.rotateClockwise();

		int orientationOffset = -1;
		switch (orientation) {
		case UP:
			orientation = Orientation.LEFT;
			orientationOffset = 3;
			break;
		case DOWN:
			orientation = Orientation.RIGHT;
			orientationOffset = 1;
			break;
		case LEFT:
			orientation = Orientation.DOWN;
			orientationOffset = 2;
			break;
		case RIGHT:
			orientation = Orientation.UP;
			orientationOffset = 0;
			break;
		}
		if (orientationOffset == -1) {
			currentTetromino = copy;
			orientation = originalOrientation;
			return;

		}

		for (int i = 0; i < 5; i++) {
			System.out.println("DEBUG: originalOffset = ["
					+ Integer.toString(minoOffset[0]) + ", "
					+ Integer.toString(minoOffset[1]) + "]");
			currentTetromino.setOffset(minoOffset[0] - this.threeWideSRSOffset[orientationOffset][i][0],
					minoOffset[1] - this.threeWideSRSOffset[orientationOffset][i][1]);
			System.out.println("DEBUG: offset = ["
					+ Integer.toString(currentTetromino.getOffset()[0]) + ", "
					+ Integer.toString(currentTetromino.getOffset()[1]) + "]");
			if (!this.isColliding()) {
				return;
			}
		}

		// reaching up to this point means that every possible srs position collides
		this.currentTetromino = copy;
		this.currentTetromino.setOffset(minoOffset[0], minoOffset[1]);
		this.orientation = originalOrientation;
		return;

	}

	public void setOrientation(Orientation orientation) {
		// TODO Auto-generated method stub
		this.orientation = orientation;
	}

	public void printBoard() {
		// TODO Auto-generated method stub
		this.board.printArr();
	}

	public void printWithCurrentTetromino() {
		for (int i = board.getSize()[1] - 1; i >= 0; i--) {
			for (int j = 0; j < board.getSize()[0]; j++) {
				if (currentTetromino.getValue(j, i) != null && currentTetromino.getValue(j, i) != Mino.BLANK)
					System.out.print(currentTetromino.getValue(j, i).toString() + " ");
				else
					System.out.print(board.getValue(j, i).toString() + " ");
			}
			System.out.println();
		}
		System.out.println("Offset: " + board.getOffset()[0] + " " + board.getOffset()[1]);
		System.out.println();
	}

	public Orientation getTetrominoOrientation() {
		// TODO Auto-generated method stub
		return orientation;
	}

}
