package com.rikum.tetris;

public enum Mino {
	BLANK,
	SKY,
	BLUE,
	ORANGE,
	PURPLE,
	YELLOW,
	RED,
	GREEN,
	GRAY
}
