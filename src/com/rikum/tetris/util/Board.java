package com.rikum.tetris.util;

import java.util.ArrayList;

public class Board<T> {

	ArrayList<ArrayList<T>> board;
	int[] offset;

	public Board(int x, int y, T filler) {
		// TODO Auto-generated constructor stub
		board = new ArrayList<ArrayList<T>>();
		for (int i = 0; i < x; i++) {
			ArrayList<T> list = new ArrayList<T>();
			for (int j = 0; j < y; j++) {
				list.add(filler);
			}
			board.add(list);
		}
		offset = new int[2];
		offset[0] = 0;
		offset[1] = 0;
	}

	/**
	 * gets the size of the board in {x,y}
	 * 
	 * @return the size of the board in {x,y}
	 */
	public int[] getSize() {
		// TODO Auto-generated method stub
		int[] size = { board.size(), board.get(0).size() };
		return size;
	}

	public boolean setValue(int x, int y, T value) {
		// TODO Auto-generated method stub
		if (getSize()[0] + offset[0] > x  && 0 + offset[0] <= x 
				&& getSize()[1] + offset[1] > y  && 0 + offset[1] <= y ) {

			board.get(x - offset[0]).set(y - offset[1], value);

			return true;
		}
		return false;
	}
	
	public ArrayList<ArrayList<T>> getArrList() {
		return this.board;
	}
	
	public void setArrayList(ArrayList<ArrayList<T>> board) {
		this.board = board;
	}
	
	public boolean switchValues(int x1,int y1, int x2, int y2) {
		T value1 = this.getValue(x1, y1);
		T value2 = this.getValue(x2, y2);
		
//		System.out.println("DEBUG: " + value1.toString() + 
//				" switched with " + value2.toString());
		
		return this.setValue(x1, y1, value2) && this.setValue(x2, y2, value1);
	}

	public T getValue(int x, int y) {
		// TODO Auto-generated method stub
		try {
			return board.get(x - offset[0]).get(y - offset[1]);
		} catch (Exception e) {
			return null;
		}
	}

	public void setOffset(int x, int y) {
		// TODO Auto-generated method stub
		offset[0] = x;
		offset[1] = y;
	}

	public int[] getOffset() {
		// TODO Auto-generated method stub
		return offset;
	}

	public void rotateClockwise() {
		// TODO Auto-generated method stub
		
		for (int i = 0; i < board.size() / 2; i++) {
			for (int j = i; j < board.get(0).size() - i -1; j++) {
				T placeholder = this.getValue(i, j);
//				System.out.println("i1,j1 =" + i + ", " + j);
//				System.out.println("i2,j2 =" + Integer.toString(board.get(0).size() - j - 1) + ", " + i);
//				System.out.println("i3,j3 =" + Integer.toString(board.size() - i - 1) + ", " + Integer.toString(board.get(0).size() - j - 1));
//				System.out.println("i4,j4 =" + j + ", " + Integer.toString(board.size() - i - 1));
//				System.out.println();
				
				this.switchValues(i + offset[0], j + offset[1], j+ offset[0], board.size() - i+ offset[1] - 1);
				this.switchValues(j + offset[0], board.size() - i - 1+ offset[1], board.size() - i - 1+ offset[0], board.get(0).size()- j - 1 + offset[1]);
				this.switchValues(board.size() - i - 1+ offset[0], board.get(0).size()- j - 1 + offset[1], board.get(0).size() - j - 1 + offset[0], i+ offset[1]);
				//this.switchValues(board.get(0).size() - j - 1, i, i, j);
				
			}
		}
		
	}

	public void rotateCounterClockwise() {
		// TODO Auto-generated method stub
		for (int i = 0; i < board.size() / 2; i++) {
			for (int j = i; j < board.get(0).size() - i -1; j++) {
				T placeholder  = this.getValue(i, j);

				this.switchValues(i + offset[0], j+ offset[1], board.get(0).size() - j - 1+ offset[0], i+ offset[1]);
				this.switchValues(board.get(0).size() - j - 1+ offset[0], i+ offset[1], board.size()- i - 1+ offset[0],board.get(0).size() - j - 1 + offset[1]);
				this.switchValues(j+ offset[0], board.size() - i - 1+ offset[1], board.size() - 1 - i+ offset[0], board.get(0).size() - 1 - j+ offset[1]);
				
			}
		}
	}

	public Board<T> copy() {
		// TODO Auto-generated method stub
		Board<T> output = new Board<T>(this.getSize()[0],this.getSize()[1], null);
		output.setOffset(this.offset[0], this.offset[1]);
		for (int i = 0; i < this.getSize()[0]; i++) {
			for (int j = 0; j < this.getSize()[1]; j++) {
				output.setValue(i+ offset[0], j+ offset[1], this.getValue(i+ offset[0], j+ offset[1]));
			}
		}
		
		return output;
	}
	
	public void printArr() {
		for (int i = board.get(0).size() - 1; i >= 0; i--) {
			for( int j = 0; j < board.size(); j++) {
				System.out.print(board.get(j).get(i).toString() + " ");
			}
			System.out.println();
		}
		System.out.println("Offset: " + offset[0] + " " + offset[1]);
		System.out.println();
	}

}
