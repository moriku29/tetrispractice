package com.rikum.tetris;

public enum Orientation {
	UP,
	DOWN,
	LEFT,
	RIGHT
}
