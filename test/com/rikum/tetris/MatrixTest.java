package com.rikum.tetris;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import static org.junit.Assert.assertTrue;

import java.util.Queue;

import org.junit.Test;

import com.rikum.tetris.util.Board;

public class MatrixTest {

	@Test
	public void contructorTest1() {
		Matrix matrix = new Matrix();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 40; j++) {
				assertEquals(Mino.BLANK, matrix.getValue(i, j));
			}
		}
	}

	@Test
	public void contructorTest2() {
		Matrix matrix = new Matrix(4, 40);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 40; j++) {
				assertEquals(Mino.BLANK, matrix.getValue(i, j));
			}
		}
	}

	/**
	 * Generates the 7-bag Tetromino randomizer. We only care about the 7 pieces
	 * first generated.
	 */
	@Test
	public void generateTetrominoSequence1() {
		Matrix matrix = new Matrix();
		Queue<Mino> list = matrix.getNextMinoList();

		for (int i = 0; i < list.size(); i++) {
			Mino pulled = list.poll();
			assertFalse(list.contains(pulled));

		}

	}

	/**
	 * Generates the 7-bag Tetromino randomizer. We now care about the 7 pieces that
	 * are generated after the first.
	 */
	@Test
	public void generateTetrominoSequence2() {
		Matrix matrix = new Matrix();
		Queue<Mino> list = matrix.getNextMinoList();
		list.poll();

		for (int i = 6; i < list.size(); i++) {
			Mino pulled = list.poll();
			assertFalse(list.contains(pulled));

		}
		assertEquals(6, list.size());

	}

	@Test
	public void collidesTest1() {
		Matrix matrix = new Matrix();
		for (int i = 0; i < 10; i++) {
			matrix.setMino(i, 0, Mino.GRAY);
		}
		Board<Mino> mino = new Board<Mino>(1, 1, Mino.BLUE);
		mino.setOffset(4, 20);
		matrix.setCurrentTetromino(mino);
		assertFalse(matrix.isColliding());
	}

	@Test
	public void collidesTest2() {
		Matrix matrix = new Matrix();
		for (int i = 0; i < 10; i++) {
			matrix.setMino(i, 0, Mino.GRAY);
		}
		Board<Mino> mino = new Board<Mino>(1, 1, Mino.BLUE);
		mino.setOffset(4, 0);
		matrix.setCurrentTetromino(mino);
		assertTrue(matrix.isColliding());
	}
	
	@Test
	public void collidesTest3() {
		Matrix matrix = new Matrix();
		for (int i = 0; i < 10; i++) {
			matrix.setMino(i, 0, Mino.GRAY);
		}
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLUE);
		mino.setOffset(-3, 3);
		matrix.setCurrentTetromino(mino);
		assertTrue(matrix.isColliding());
	}

	@Test
	public void hardDropTest1() throws Exception {
		Matrix matrix = new Matrix();

		Board<Mino> mino = new Board<Mino>(1, 1, Mino.BLUE);
		mino.setOffset(4, 20);
		matrix.setCurrentTetromino(mino);

		matrix.performHardDrop();
		assertEquals(Mino.BLUE, matrix.getValue(4, 0));
	}

	@Test
	public void hardDropTest2() throws Exception {
		Matrix matrix = new Matrix();
		for (int i = 0; i < 10; i++) {
			matrix.setMino(i, 0, Mino.GRAY);
		}
		Board<Mino> mino = new Board<Mino>(1, 1, Mino.BLUE);
		mino.setOffset(4, 20);
		matrix.setCurrentTetromino(mino);

		matrix.performHardDrop();
		assertEquals(Mino.BLUE, matrix.getValue(4, 1));
	}

	@Test
	public void currentTetrominoPlacementTest1() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(2, 1, Mino.PURPLE);
		mino.setOffset(-1, 10);
		matrix.setCurrentTetromino(mino);
		assertFalse(matrix.isColliding());
	}

	@Test
	public void currentTetrominoPlacementTest2() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(2, 1, Mino.PURPLE);
		mino.setOffset(-2, 10);
		matrix.setCurrentTetromino(mino);
		assertTrue(matrix.isColliding());
		
	}

	@Test
	public void moveTetrominoRightTest() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(2, 1, Mino.PURPLE);
		mino.setOffset(4, 10);
		matrix.setCurrentTetromino(mino);

		while (matrix.moveTetrominoRight())
			;
		assertEquals(7, matrix.getCurrentTetromino().getOffset()[0]);
	}

	@Test
	public void moveTetrominoLeftTest() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(2, 1, Mino.PURPLE);
		mino.setOffset(4, 10);
		matrix.setCurrentTetromino(mino);

		while (matrix.moveTetrominoLeft())
			;
		assertEquals(-1, matrix.getCurrentTetromino().getOffset()[0]);
	}

	@Test
	public void moveTetrominoDownTest() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(2, 1, Mino.PURPLE);
		mino.setOffset(4, 10);
		matrix.setCurrentTetromino(mino);

		while (matrix.moveTetrominoDown())
			;
		assertEquals(0, matrix.getCurrentTetromino().getOffset()[1]);
	}

	@Test
	public void rotateRightTest1() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(0, 1, Mino.PURPLE);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setOffset(4, 10);
		matrix.setCurrentTetromino(mino);
		
		matrix.getCurrentTetromino().printArr();
		matrix.turnTetrominoRight();
matrix.getCurrentTetromino().printArr();
		
		Board<Mino> resultmino = new Board<Mino>(3, 3, Mino.BLANK);
		resultmino.setValue(1,2, Mino.PURPLE);
		resultmino.setValue(1, 1, Mino.PURPLE);
		resultmino.setValue(0, 1, Mino.PURPLE);
		resultmino.setValue(2, 1, Mino.PURPLE);
		resultmino.setOffset(4, 10);
		
		matrix.getCurrentTetromino().printArr();
		resultmino.printArr();
		
		for (int i = 4; i < 7; i++) {
			for (int j = 10; j < 13; j++) {
				//System.out.println("DEBUG: resultMino at " + i + ", " + j + ": " + resultmino.getValue(i, j));
				assertEquals("i = " + i + ", j = " + j, resultmino.getValue(i, j),
						matrix.getCurrentTetromino().getValue(i, j));
			}
		}

	}
	
	@Test
	public void rotateRightTest2() {
		Matrix matrix = new Matrix();
		matrix.setMino(9, 0, Mino.GRAY);
		matrix.setMino(9, 1, Mino.GRAY);
		matrix.setMino(9, 2, Mino.GRAY);
		matrix.setMino(8, 0, Mino.GRAY);
		matrix.setMino(8, 2, Mino.GRAY);
		matrix.setMino(7, 4, Mino.GRAY);
		matrix.setMino(6, 0, Mino.GRAY);
		matrix.setMino(6, 1, Mino.GRAY);
		matrix.setMino(6, 2, Mino.GRAY);
		matrix.setMino(6, 3, Mino.GRAY);
		matrix.setMino(6, 4, Mino.GRAY);
		
		//matrix.printBoard();
		
		Board<Mino> mino =  new Board<Mino>(3,3,Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(0, 1, Mino.PURPLE);
		mino.setOffset(8, 3);
		matrix.setCurrentTetromino(mino);
		matrix.setOrientation(Orientation.LEFT);
		
		matrix.getCurrentTetromino().printArr();
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		matrix.turnTetrominoRight();
		
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		
		matrix.getCurrentTetromino().printArr();
		assertEquals(7,matrix.getCurrentTetromino().getOffset()[0]);
		assertEquals(2,matrix.getCurrentTetromino().getOffset()[1]);
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		matrix.turnTetrominoRight();
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		matrix.getCurrentTetromino().printArr();
		assertEquals(6,matrix.getCurrentTetromino().getOffset()[0]);
		assertEquals(0,matrix.getCurrentTetromino().getOffset()[1]);
		
		
		matrix.turnTetrominoRight();
		matrix.getCurrentTetromino().printArr();
		assertEquals(6,matrix.getCurrentTetromino().getOffset()[0]);
		assertEquals(0,matrix.getCurrentTetromino().getOffset()[1]);
	}
	
	
	
	@Test
	public void rotateLeftTest1() {
		Matrix matrix = new Matrix();
		Board<Mino> mino = new Board<Mino>(3, 3, Mino.BLANK);
		mino.setValue(0, 1, Mino.PURPLE);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setOffset(4, 10);
		matrix.setCurrentTetromino(mino);
		
		matrix.turnTetrominoLeft();
//	matrix.getCurrentTetromino().printArr();
		
		Board<Mino> resultmino = new Board<Mino>(3, 3, Mino.BLANK);
		resultmino.setValue(1,0, Mino.PURPLE);
		resultmino.setValue(1, 1, Mino.PURPLE);
		resultmino.setValue(0, 1, Mino.PURPLE);
		resultmino.setValue(2, 1, Mino.PURPLE);
		resultmino.setOffset(4, 10);
		
	//	matrix.getCurrentTetromino().printArr();
		//resultmino.printArr();
		
		for (int i = 4; i < 7; i++) {
			for (int j = 10; j < 13; j++) {
				//System.out.println("DEBUG: resultMino at " + i + ", " + j + ": " + resultmino.getValue(i, j));
				assertEquals("i = " + i + ", j = " + j, resultmino.getValue(i, j),
						matrix.getCurrentTetromino().getValue(i, j));
			}
		}

	}
	
	@Test
	public void rotateLeftTest2() {
		Matrix matrix = new Matrix();
		matrix.setMino(0, 0, Mino.GRAY);
		matrix.setMino(0, 1, Mino.GRAY);
		matrix.setMino(0, 2, Mino.GRAY);
		matrix.setMino(1, 0, Mino.GRAY);
		matrix.setMino(1, 2, Mino.GRAY);
		matrix.setMino(2, 4, Mino.GRAY);
		matrix.setMino(3, 0, Mino.GRAY);
		matrix.setMino(3, 1, Mino.GRAY);
		matrix.setMino(3, 2, Mino.GRAY);
		matrix.setMino(3, 3, Mino.GRAY);
		matrix.setMino(3, 4, Mino.GRAY);
		
		//matrix.printBoard();
		
		Board<Mino> mino =  new Board<Mino>(3,3,Mino.BLANK);
		mino.setValue(1, 0, Mino.PURPLE);
		mino.setValue(1, 1, Mino.PURPLE);
		mino.setValue(1, 2, Mino.PURPLE);
		mino.setValue(2, 1, Mino.PURPLE);
		mino.setOffset(-1, 3);
		matrix.setCurrentTetromino(mino);
		matrix.setOrientation(Orientation.RIGHT);
		
		matrix.getCurrentTetromino().printArr();
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		matrix.turnTetrominoLeft();
		
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		
		matrix.getCurrentTetromino().printArr();
		assertEquals(0,matrix.getCurrentTetromino().getOffset()[0]);
		assertEquals(2,matrix.getCurrentTetromino().getOffset()[1]);
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		matrix.turnTetrominoLeft();
		System.out.println("======================");
		matrix.printWithCurrentTetromino();
		System.out.println("======================");
		matrix.getCurrentTetromino().printArr();
		assertEquals(1,matrix.getCurrentTetromino().getOffset()[0]);
		assertEquals(0,matrix.getCurrentTetromino().getOffset()[1]);
		
		
		matrix.turnTetrominoLeft();
		matrix.getCurrentTetromino().printArr();
		assertEquals(1,matrix.getCurrentTetromino().getOffset()[0]);
		assertEquals(0,matrix.getCurrentTetromino().getOffset()[1]);
		assertEquals(Orientation.LEFT, matrix.getTetrominoOrientation());
	}

}
