package com.rikum.tetris.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.rikum.tetris.Mino;

public class BoardTest {

	@Test
	public void constructorTest() {
		Board<Boolean> board = new Board<Boolean>(2, 4, false);
		int[] size = { 2, 4 };
		assertArrayEquals(size, board.getSize());
	}

	@Test
	public void setAndGetTest1() {
		Board<Boolean> board = new Board<Boolean>(4, 4, false);
		board.setValue(0, 0, true);
		board.setValue(1, 1, true);
		board.setValue(2, 2, true);
		board.setValue(3, 3, true);

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i == j)
					assertTrue(board.getValue(i, j));
				else
					assertFalse(board.getValue(i, j));
			}
		}

	}

	@Test
	public void setAndGetTest2() {
		Board<Boolean> board = new Board<Boolean>(4, 4, false);
		assertTrue(board.setValue(0, 0, false));
		assertFalse(board.setValue(4, 4, false));
	}

	@Test
	public void setOffsetTest1() {
		Board<Boolean> board = new Board<Boolean>(4, 4, false);
		board.setValue(0, 0, true);
		board.setOffset(-1, -1);
		assertTrue(board.getValue(-1, -1));
		assertNull(board.getValue(3, 3));
		assertFalse(board.setValue(3, 3, true));
	}

	@Test
	public void rotateClockwiseTest() {
		Board<Integer> board = new Board<Integer>(4, 4, -1);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				board.setValue(j, i, i * 4 + j + 1);
				System.out.print(i * 4 + j + 1 + " ");
			}
			System.out.println();
		}
		board.rotateClockwise();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(i + 12 - j * 4 + 1 + " ");
				assertEquals(i + 12 - j * 4 + 1, board.getValue(j, i).intValue());
			}
			System.out.println();
		}

	}
	
	@Test
	public void rotateClockwiseTest2() {
		Board<Integer> board = new Board<Integer>(4, 4, -1);
		board.setOffset(1, 1);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				board.setValue(j+1, i+1, i * 4 + j + 1);
				System.out.print(i * 4 + j + 1 + " ");
			}
			System.out.println();
		}
		
		board.rotateClockwise();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(i + 12 - j * 4 + 1 + " ");
				assertEquals(i + 12 - j * 4 + 1, board.getValue(j + 1, i + 1).intValue());
			}
			System.out.println();
		}
		board.printArr();
	}

	@Test
	public void rotateCounterClockwiseTest() {
		Board<Integer> board = new Board<Integer>(4, 4, -1);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				board.setValue(j, i, i * 4 + j + 1);
				System.out.print(i * 4 + j + 1 + " ");
			}
			System.out.println();
		}
		board.rotateCounterClockwise();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(4 * (j+1) - i + " ");
				assertEquals(4 * (j+1) - i, board.getValue(j, i).intValue());
			}
			System.out.println();
		}

	}
	
	@Test
	public void rotateCounterClockwiseTest2() {
		Board<Integer> board = new Board<Integer>(4, 4, -1);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				board.setValue(j, i, i * 4 + j + 1);
				System.out.print(i * 4 + j + 1 + " ");
			}
			System.out.println();
		}
		board.setOffset(1, 1);
		board.rotateCounterClockwise();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(4 * (j+1) - i + " ");
				assertEquals(4 * (j+1) - i, board.getValue(j+1, i+1).intValue());
			}
			System.out.println();
		}

	}
}
